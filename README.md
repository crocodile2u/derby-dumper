# derby-dumper

1. Mount your databases root folder as /dbs
2. Specify database folder, database name and optionally tables to dump
3. For a MySQL-compatible dump, set environment variable MYSQL=1

Example:
```bash
$ pwd
/home/victor/databases
$ ls
db1 db2
$ docker run --rm -e MYSQL=1 -v $(pwd):/dbs crocodile2u/derby-dumper db1 APP TABLE1 TABLE2 ...
```
