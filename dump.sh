#!/bin/sh

exit_on_error()
{
  if [ $? -ne "0" ]; then
    echo $1
    exit 1
  fi
}

DBFOLDER="$1"
shift

TMPDIR="/tmp"
DSN="jdbc:derby:/dbs/$DBFOLDER"

TABLES="$*"

ALL_TABLES=0

if [ -z "$TABLES" ]; then
  ALL_TABLES=1
  TABLES=$(printf "connect '%s';\nshow tables in APP;" "$DSN" | ij 2>/dev/null | grep "^APP" | awk '{print $2}' | sed 's/|//g')
fi

if [ -z "$TABLES" ]; then
  echo "Nothing to dump"
  exit 1
fi

if [ -z "$FORMAT" ]; then
  FORMAT="sql"
fi

if [ "$FORMAT" != "sql" ]; then
  NO_SCHEMA=1
fi

if [ -z "$NO_SCHEMA" ]; then

  if [ $ALL_TABLES -eq "0" ]; then
    SCHEMA=$(dblook -d "$DSN" -t $TABLES 2>/dev/null | sed -e "s/\"APP\".//g")
  else
    SCHEMA=$(dblook -d "$DSN" 2>/dev/null | sed -e "s/\"APP\".//g")
  fi

  exit_on_error "failed to dump schema"

  MYSQL_QUOTES=0
  [ -z "$MYSQL" ] || MYSQL_QUOTES=1

  if [ $MYSQL_QUOTES -eq "0" ]; then
    echo "$SCHEMA"
  else
    mysqlcompat.php "$SCHEMA"
  fi

  exit_on_error "failed to dump schema (postprocessing failed)"
fi

for TABLE in $TABLES
do

#	printf "connect '%s';\nCALL SYSCS_UTIL.SYSCS_EXPORT_TABLE (null,'%s','%s/data.del',null,null,null);" "$DSN" "$TABLE" "$TMPDIR"
	printf "connect '%s';\nCALL SYSCS_UTIL.SYSCS_EXPORT_TABLE (null,'%s','%s/data.del',null,null,null);" "$DSN" "$TABLE" "$TMPDIR" | ij > /dev/null 2>&1
	exit_on_error "failed to dump table $TABLE"

  case "$FORMAT" in
    "sql")

      printf "\n-- dumping DB data\n";
      printf "BEGIN;\n";

      cat "$TMPDIR/data.del" | inserts.php
      exit_on_error "failed converting derby CSV to SQL for $TABLE"

      printf "COMMIT;\n";
	    ;;
	  "json")
      cat "$TMPDIR/data.del" | json.php --dsn "$DSN" --columns-from "$TABLE"
      exit_on_error "failed converting derby CSV to JSON for $TABLE"
      ;;

  esac

  rm "$TMPDIR/data.del"
done
