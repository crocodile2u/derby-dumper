<?php

declare(strict_types=1);

namespace DerbyCsv;

class Parser {

    const STR = 1;
    const NUMBER = 2;

    /**
     * @var resource
     */
    private $fp;

    /**
     * @var string[]
     */
    private $header;

        /**
     * @param resource $fp
     */
    public static function fromStream($fp): self
    {
        $p = new self;
        $p->fp = $fp;
        return $p;
    }

    /**
     * @param string $filename
     */
    public static function fromFile(string $filename): self
    {
        return self::fromStream(fopen($filename, "r"));
    }

    /**
     * @param string[] $header
     * @return $this
     */
    public function setHeader(string ...$columns): Parser
    {
        $this->header = $columns;
        return $this;
    }

    public function parse()
    {
        $row = [];
        /** @var string $cell */
        $cell = null;
        $type = null;
        $prev = null;
        $closed = false;
        while (!feof($this->fp)) {
            $c = fread($this->fp, 1);
            if ($closed && !in_array($c, [",", "\n", "\r", ""])) {
                throw new \RuntimeException("expected comma after string field was closed, got $c");
            }
            switch ($c) {
                case '"':
                    if ($type === self::STR) {
                        if ($prev === '"') {
                            $cell .= '"';
                        } else {
                            $closed = true;
                        }
                    } else {
                        $cell = "";
                        $c = "";// so that quote is not written to prev
                        $type = self::STR;
                    }
                    break;
                case ",":
                    if ($closed) {
                        $row[] = $cell;
                        $closed = false;
                    } elseif ($type === self::STR) {
                        $cell .= ",";
                        break;
                    } else {
                        if (null === $cell) {
                            $row[] = null;
                        } else {
                            $row[] = $this->number($cell);
                        }
                    }
                    $cell = null;
                    $type = null;
                    break;
                case "\n":
                case "\r":
                case "":
                    if ($closed) {
                        $row[] = $cell;
                        $closed = false;
                    } elseif ($type === self::STR) {
                        $cell .= $c;
                        break;
                    } elseif (null === $cell) {
                        if ($prev === ",") {
                            $row[] = null;
                        }
                    } else {
                        $row[] = $this->number($cell);
                    }

                    if (count($row)) {
                        $yield = $this->header ? array_combine($this->header, $row) : $row;
                        yield $yield;
                    }

                    $row = [];
                    $cell = null;
                    $type = null;
                    break;
                default:
                    if ($cell === null) {
                        $cell = "";
                    }

                    $cell .= $c;
                    if ($type === null) {
                        $type = self::NUMBER;
                    }
                    break;
            }
            $prev = $c;
        }
    }

    private function number(string $val)
    {
        if (substr_count($val, ".")) {
            return (float)$val;
        } else {
            return (int)$val;
        }
    }
}