<?php

namespace Tests\DerbyCsv;

use DerbyCsv\Parser;
use PHPUnit\Framework\TestCase;

class ParserTest extends TestCase {
    /**
     * @param string $csv
     * @param array $expected
     * @dataProvider provideParseInput
     */
    public function testParse(string $csv, array $expected)
    {
        $fp = fopen('php://memory','r+');
        fwrite($fp, $csv);
        rewind($fp);

        $parser = Parser::fromStream($fp);
        $rows = iterator_to_array($parser->parse());
        $this->assertEquals($expected, $rows);
    }

    public function testHeader()
    {
        $fp = fopen('php://memory','r+');
        fwrite($fp, "0,2,3");
        rewind($fp);

        $parser = Parser::fromStream($fp)->setHeader("c1", "c2", "c3");
        $rows = iterator_to_array($parser->parse());
        $expected = [
            [
                "c1" => 0,
                "c2" => 2,
                "c3" => 3,
            ]
        ];
        $this->assertEquals($expected, $rows);
    }

    public function provideParseInput()
    {
        return [
            [
                "0,1",
                [
                    [0, 1]
                ],
            ],
            [
                "0,1\n",
                [
                    [0, 1]
                ],
            ],
            [
                "0,1.1,0.2,5",
                [
                    [0,1.1,0.2,5]
                ],
            ],
            [
                ",0,\"\"",
                [
                    [null, 0, ""]
                ],
            ],
            [
                '"str"',
                [
                    ["str"]
                ],
            ],
            [
                "\"multi\nline\"",
                [
                    ["multi\nline"]
                ],
            ],
            [
                '"line 1",25,"kg"' . "\n" . '"line 2",43,"g"',
                [
                    ["line 1", 25, "kg"],
                    ["line 2", 43, "g"],
                ],
            ],
        ];
    }
}