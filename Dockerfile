FROM lpicanco/java11-alpine

RUN apk add php php-json && \
    wget -O /derby.tar.gz http://apache.cs.uu.nl/db/derby/db-derby-10.15.1.3/db-derby-10.15.1.3-bin.tar.gz && \
    cd / && \
    tar -xzf derby.tar.gz && \
    mv db-* derby && \
    rm *.tar.gz

RUN mkdir /dbs
VOLUME /dbs

WORKDIR /dbs
ENV PATH="/derby/bin:${PATH}"

COPY dump.sh /derby/bin/
COPY query.sh /derby/bin/
COPY vendor /derby/bin/vendor
COPY inserts.php /derby/bin/
COPY mysqlcompat.php /derby/bin/
COPY json.php /derby/bin/

ENTRYPOINT ["dump.sh"]
