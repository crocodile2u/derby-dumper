#!/usr/bin/php
<?php

$schema = $_SERVER['argv'][1] ?? null;

if (empty($schema)) {
    echo "empty input";
    exit(1);
}

$schema = str_replace('"', '`', $schema);
$schema = preg_replace_callback("/CLOB\((\d+)\)/", function ($m) {
    $length = $m[1];
    if ($length <= 255) {
        return "TINYTEXT";
    } elseif ($length <= 65535) {
        return "TEXT";
    } elseif ($length <= 16777215) {
        return "MEDIUMTEXT";
    } else {
        return "LONGTEXT";
    }
}, $schema);

$maxVarcharLen = (int)getenv("MAX_VARCHAR");
if ($maxVarcharLen > 0) {
    $schema = preg_replace_callback("/VARCHAR\((\d+)\)/", function ($m) use ($maxVarcharLen) {
        if ($m[1] > $maxVarcharLen) {
            return "VARCHAR($maxVarcharLen)";
        } else {
            return "VARCHAR({$m[1]})";
        }
    }, $schema);
}

echo $schema;