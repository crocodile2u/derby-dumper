#!/usr/bin/php
<?php

include_once __DIR__ . "/vendor/autoload.php";

$parser = \DerbyCsv\Parser::fromStream(STDIN);

foreach ($parser->parse() as $line) {
    $sqlFields = array_map(function ($v) {
        switch (gettype($v)) {
            case "string":
                return "'" . addslashes($v) . "'";
            case "NULL":
                return "NULL";
            default:
                return $v;
        }
    }, $line);
    echo "INSERT INTO $t VALUES (".join(", ", $sqlFields).");\n";
}