#!/bin/sh

DBFOLDER="$1"

QUERY="$2"

shift
shift

COLUMNS="$*"

TMPDIR="/tmp"
DSN="jdbc:derby:/dbs/$DBFOLDER"

exit_on_error()
{
  if [ $? -ne "0" ]; then
    echo $1
    exit 1
  fi
}

QUERY=$(echo "$QUERY" | sed "s/'/''/g")
printf "connect '%s';\nCALL SYSCS_UTIL.SYSCS_EXPORT_QUERY('%s','%s/query.del',null,null,null);" "$DSN" "$QUERY" "$TMPDIR" | ij > /dev/null 2>&1
exit_on_error "query execution failed"

case "$FORMAT" in
  "json")
    cat "$TMPDIR/query.del" | json.php --columns "$COLUMNS"
    exit_on_error "failed to convert query results to JSON"
    ;;
esac

rm "$TMPDIR/query.del"
