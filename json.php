#!/usr/bin/php
<?php

$opts = getopt("", ["columns-from:", "dsn:", "columns:"]);

$dsn = $opts["dsn"] ?? null;

$table = $opts["columns-from"] ?? null;
$cols = [];
if ($table) {
    $cmd = "echo \"connect '$dsn';DESCRIBE $table;\" | ij 2>/dev/null";
    exec($cmd, $output, $status);
    if ($status) {
        echo "Cannot describe $table: ij exited with status $status\n";
        exit(1);
    }

    $started = false;
    foreach ($output as $line) {
        if ($started) {
            if (!trim($line)) {
                break;
            }
            $ws = strpos($line, " ");
            $cols[] = substr($line, 0, $ws);
        } elseif (preg_match("/^\-+$/", $line)) {
            $started = true;
        }
    }
} else {
    $opt = trim($opts["columns"] ?? "");
    if ($opt) {
        $cols = preg_split("/\s+/", $opt);
    }
}

include_once __DIR__ . "/vendor/autoload.php";

$parser = \DerbyCsv\Parser::fromStream(STDIN);
if ($cols) {
    $parser->setHeader(...$cols);
}

foreach ($parser->parse() as $line) {
    echo json_encode($line) . "\n";
}